#!/bin/bash
set -e
#cd content
POST_SLUG="$1"
if [ -z "$POST_SLUG" ]; then
  read -p "Post Name (e.g. your-new-post): " POST_SLUG
fi
current_time=$(date "+%Y.%m.%d-%H.%M.%S")
echo "Current Time : $current_time"
TIMESTAMP=`date +%Y%m%d%H%M%S`
POST_FILENAME="${TIMESTAMP}-${POST_SLUG}.md"
hugo new "posts/${POST_FILENAME}"
#perl -pi -e 's/slug = ""/slug = "\'${POST_SLUG}'\"/g' content/posts/${POST_FILENAME}
sed -i '6s/slug = ""/slug = "'${POST_SLUG}'"/' content/posts/${POST_FILENAME}

