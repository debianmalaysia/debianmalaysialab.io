---
draft = true
date = {{ .Date }}
title = ""{{ .TranslationBaseName | replaceRE "^[0-9]{14}-" "" | replaceRE "-" " " | title }}""
description = ""
slug = {{ .TranslationBaseName | replaceRE "^[0-9]{14}-" ""  }}
authors = []
tags = []
categories = []
externalLink = ""
series = []
images = ["/images/debian_transparent.png"]
---