+++ 
draft = false
date = 2021-05-27T09:06:46+08:00
title = "Debian-Malaysia now reachable on OFTC and KampungChat network"
description = ""
slug = "oftc-kampungchat"
authors = ['Robbi Nespu <robbinespu AT SPAMFREE gmail DOT com>']
tags = ['news','irc','matrix','telegram','debian malaysia','oftc','chats','kampungchat','freenode']
categories = []
externalLink = ""
series = []
+++

Assalamualaikum and hello Malaysian 🇲🇾

Have you heard the recent news about freenode staff resigned because freenode IRC network has been taken over by a korean royalty bitcoins millionaire 👀

I would like to take this `freenode`, `libera.chat`, `irc` trending keywords opportunity to inform you that we (debian malaysia localgroup) have registered our channel `#debian-malaysia` on KampungChat and OFTC network!

If you don't have IRC client yet, I suggest you to use GUI application such as hexchat, you may use terminal command below for hexchat package installation

```bash
$ sudo apt install hexchat hexchat-plugins hexchat-otr
```

Now come and join us on `OFTC` or `KampungChat` network. You may use the details below for access
```
OFTC
====
server  : irc.debian.org / irc.oftc.net (matrix: #_oftc_#debian-malaysia:matrix.org)
port    :   - 6697 for SSL (alternative port: 9999), IPv4 and IPv6
            - 6667 for non-SSL (alternative ports: 6668-6670, 7000), IPv4 and IPv6

KampungChat
===========
server  : irc.kampungchat.org / nexus.kaos.my / irc.tengkorak.org
port    : 6667-7000,6697[SSL]
```
See you on the net 🥰
.

.

.

.

.

.

.

.


## OFTC vs Freenode vs Libera.chat
Let talk a bit about OFTC (Open and Free Technology Community) is a member project of Software in the Public Interest, a non-profit organization which was founded to help organizations develop and distribute open hardware and software. Which likely a sister organisation of Debian, as both are supported and represented by Software in the Public Interest, Inc. OFTC is where official debian IRC host alias are pointing since June 4th, 2006 after the Debian project decide to move from Freenode to OFTC.

Most people still not aware and notice that `#debian*` channel on Freenode are not the official channel, but the numbers of people gathering on debian channel on freenode network are more higher and active if we compare to OFTC network.

Freenode is the best place where we (debian malaysia localgroup) can find Malaysian opensource advocate user on IRC network. That why we have our own channel `#debian-malaysia` on Freenode and not on OFTC previously.
 
Freenode is a Free Software friendly IRC network that has been providing the IRC service for many opensource commmunity but recently after handover to new owner, it likely alots of channel are unilaterally taken over, hijacked and more 💩. It make sense to say it not friendly anymore. Sadly, unoficial debian and most people IRC move to Libera.chat instead of OFTC. I don't know why, but for me it great oppurtunity to move and start using our official IRC network, but nevermind.

I tried to register `#debian-malaysia` on Libera.chat but it seem channel registeration on some namespace take more step and procedure. You may read my chat log with #debian-localgroup folk [here](https://paste.debian.net/plain/1198961) for more details.

So here we are, I just registered `#debian-malaysia` on OFTC and bridge it to our telegram group

```
May 27 00:39:44 >chanserv<	REGISTER #debian-malaysia "Malaysian Debian GNU/Linux enthusiasts community"
May 27 00:39:45 ◄ChanServ►	Channel #debian-malaysia successfully registered.
May 27 00:40:26 >chanserv<	SET #debian-malaysia URL https://wiki.debian.org/LocalGroups/Debian-Malaysia
May 27 00:40:27 ◄ChanServ►	URL has been set to https://wiki.debian.org/LocalGroups/Debian-Malaysia on channel #debian-malaysia.
May 27 00:41:25 >chanserv<	SET #debian-malaysia EMAIL https://groups.google.com/g/debmal
May 27 00:41:25 ◄ChanServ►	EMAIL has been set to https://groups.google.com/g/debmal on channel #debian-malaysia.
May 27 00:41:50 >chanserv<	ACCESS #debian-malaysia ADD @debian-master MASTER
May 27 00:41:50 ◄ChanServ►	@debian-master added to #debian-malaysia access list as MASTER.
May 27 00:42:10 >chanserv<	ACCESS #debian-malaysia ADD @debian-chanop CHANOP
May 27 00:42:11 ◄ChanServ►	@debian-chanop added to #debian-malaysia access list as CHANOP.
May 27 00:42:56 >chanserv<	ACCESS #debian-malaysia LIST
May 27 00:42:56 ◄ChanServ►	1: @debian-master MASTER
May 27 00:42:56 ◄ChanServ►	2: @debian-chanop CHANOP
May 27 00:42:56 ◄ChanServ►	3: rnm MASTER
May 27 00:42:56 ◄ChanServ►	End of ACCESS LIST for channel #debian-malaysia.
```

 You may access OFTC network using irc or matrix protocol, to me this is one of advantage of OTFC which it officially bridge by `matrix.org`

 ## KampungChat - The Best Malaysian IRC Network
 I almost forgot the biggest Malaysian IRC network community which are `KampungChat`! Most of us using this network to hangout, play trivia / games, request song, find lovers and more. The famous channel are `#Kampung` and `#mamak` which full of Malaysian user hangout on the net! The administrative also do community gathering (before pandemic) for eyeball physically between users.

 The given [policy](https://www.kampungchat.org.my/wiki/Irc_polisi) also good enough for us, so I also created `#debian-malaysia` channel on KampungChat network. Thanks to KampungChat policy which allows bot and not blocking my vps IP server, I able to put a bot and bridge with telegram group. Horray!

```
**** BEGIN LOGGING AT Wed May 26 17:29:38 2021
May 26 17:39:25 >chanserv<	register #debian-malaysia
May 26 17:39:25 ◄ChanServ►	Channel #debian-malaysia registered under your account: rnm
May 26 17:39:25 ━━▶ Joins:	KC (bot@bot.kampungchat.org)
May 26 17:39:25 ❨❢❩	KC sets mode +r #debian-malaysia
May 26 17:39:25 ❮▲❯	KC gives channel operator status to KC
May 26 17:40:45 >ChanServ<	SET EMAIL debianmalaysia@gmail.com
May 26 17:40:46 ◄ChanServ►	Channel debianmalaysia@gmail.com isn't registered.
May 26 17:41:32 >ChanServ<	SET EMAIL #debian-malaysia debianmalaysia@gmail.com
May 26 17:41:33 ◄ChanServ►	EMAIL for #debian-malaysia set to debianmalaysia@gmail.com.
May 26 17:42:50 >ChanServ<	SET URL #debian-malaysia https://wiki.debian.org/LocalGroups/Debian-Malaysia
May 26 17:42:51 ◄ChanServ►	URL for #debian-malaysia set to https://wiki.debian.org/LocalGroups/Debian-Malaysia.
May 26 17:43:44 >ChanServ<	SET TOPICHISTORY #debian-malaysia ON
May 26 17:43:45 ◄ChanServ►	Topic history option for #debian-malaysia is now on.
May 26 17:44:47 >ChanServ<	SET DESCRIPTION #debian-malaysia "Malaysian Debian GNU/Linux enthusiasts community"
May 26 17:44:48 ◄ChanServ►	Description of #debian-malaysia changed to "Malaysian Debian GNU/Linux enthusiasts community".
May 26 17:45:40 >ChanServ<	SET CHANSTATS #debian-malaysia ON
May 26 17:45:41 ◄ChanServ►	Chanstats statistics are now enabled for this channel.
May 26 17:46:16 >ChanServ<	SET AUTOOP #debian-malaysia ON
May 26 17:46:17 ◄ChanServ►	Services will now automatically give modes to users in #debian-malaysia.
May 26 17:54:34 >ChanServ<	SET KEEPTOPIC #debian-malaysia ON
May 26 17:54:34 ◄ChanServ►	Topic retention option for #debian-malaysia is now on.
May 26 17:57:39 >chanserv<	access #debian-malaysia ADD rnm Founder
May 26 17:57:39 ◄ChanServ►	rnm added to #debian-malaysia access list at privilege FOUNDER (level 10000)
May 26 17:57:49 >chanserv<	access #debian-malaysia list
May 26 17:57:49 ◄ChanServ►	Access list for #debian-malaysia:
May 26 17:57:49 ◄ChanServ►	Number  Level  Mask
May 26 17:57:49 ◄ChanServ►	1       10000  rnm
May 26 17:57:49 ◄ChanServ►	End of access list
May 26 17:59:43 >ChanServ<	 INFO #debian-malaysia
May 26 17:59:44 ◄ChanServ►	Information for channel #debian-malaysia:
May 26 17:59:44 ◄ChanServ►	    Founder: rnm
May 26 17:59:44 ◄ChanServ►	Description: "Malaysian Debian GNU/Linux enthusiasts community"
May 26 17:59:44 ◄ChanServ►	 Registered: May 26 17:39:25 2021 +08 (20 minutes ago)
May 26 17:59:44 ◄ChanServ►	  Last used: May 26 17:59:44 2021 +08 (now)
May 26 17:59:44 ◄ChanServ►	   Ban type: 2
May 26 17:59:44 ◄ChanServ►	  Mode lock: +nt
May 26 17:59:44 ◄ChanServ►	    Options: Peace, Security, Secure founder, Secure ops, Signed kicks, Topic retention, Topic history, Chanstats
May 26 17:59:44 ◄ChanServ►	      EMAIL: debianmalaysia@gmail.com
May 26 17:59:44 ◄ChanServ►	        URL: https://wiki.debian.org/LocalGroups/Debian-Malaysia
```
