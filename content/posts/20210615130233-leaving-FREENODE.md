+++ 
draft = false
date = 2021-06-15T13:02:33+08:00
title = "We are leaving FREENODE network server"
description = ""
slug = "leaving-FREENODE"
authors = ['Robbi Nespu <robbinespu AT SPAMFREE gmail DOT com>']
tags = ['freenode','irc','matrix','chat','oftc', 'debian-malaysia']
categories = []
externalLink = ""
series = []
+++

Recently, all registered accounts and channels are lost and gone on freenode IRC network. I went to ask them on `#freenode` ask them directly what happen. Here some snip of the discussion that happened:
```log
[12:37:02 PM] <mambang> WTF is happening
[12:37:26 PM] <jane> mambang: context?
[12:37:42 PM] <fran> shouldn't at least publish an official explanation into the web?
[12:37:45 PM] <mambang> where is my acc and channel?
[12:37:49 PM] <mcluvin> anyone wanna throw punches?
[12:37:53 PM] <reeeeenode> GET REKT  SCRUB
[12:37:55 PM] <duckgoose> mambang, gone
[12:37:57 PM] <duckgoose> gotta start over
[12:38:01 PM] <chibill> I still want answers, where did my email and password data go, why was the channels unregistered, why was my nick unregistered.. And I want a staff member to give and offical answer. 
[12:38:03 PM] <tetrakist> mambang, its on the old freenode servers. This is new freenode.
[12:38:08 PM] <mambang> bamm dussss
[12:38:11 PM] <AbleBacon> all my channels are empty
[12:38:32 PM] <jane> fran: new management doesn't wish to give warning in advance, soz
[12:38:33 PM] <kaniini> this is amazing work
[12:38:33 PM] <duckgoose> there is nothing more to it
[12:38:34 PM] <reeeeenode> HollyW00d: FIX YOUR SHIT
[12:38:47 PM] <ab> mambang: you took the wrong bus.
[12:41:02 PM] <chibill> Under the GDPR I have a right to know what happened to my data and the right to have it destroyed. 
[12:41:40 PM] <mambang> ur are screw everything
[13:00:39 PM] <mambang> here a gift https://i.imgflip.com/5db07t.jpg
```
Everyone are mad and disadpointed on what happened. We previously stay on Freenode because we have more users hangout on this network compare to OFTC but now we need to restart again from scratch?

So I decide to pull out, my IRC bot and matrix bridge from this freenode server. We use OFTC and telegram group (both bridged) as primary channel and maybe one day, I will registering `#debian-malaysia` on `libera.chat` later on.

{{< figure src="/images/screw-freenode.jpg" caption="Goodbye F***node!" >}}

So long and good bye Freenode! 

Sincerly,

-- Freenode user since 2007-2021

