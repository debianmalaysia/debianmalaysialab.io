+++ 
draft = false
date = 2022-03-15T12:35:46+08:00
title = "Freenode #debian-malaysia archive log"
description = ""
slug = "freenode-irc-chat-log-archive"
authors = ['Robbi Nespu <robbinespu AT SPAMFREE gmail DOT com>']
tags = ['freenode','irc','matrix','chat','oftc', 'debian-malaysia']
categories = []
externalLink = ""
series = []
+++

Hello readers! It been a while since I posted [we are leaving FREENODE network server ](posts/leaving-freenode/) last year and we now stay in OFTC (primary) and KampungChat (mirror) IRC network.

Today, I checked something and I stumble `LogBot` which are the logging bot that I put on our channel (Freenode network). This bot was completely decommissioned in mid June 2021, nearly same as the time we left Freenode network. 

Archives of the every channel logs that use this bot can be found [here](https://archive.logbot.info/). I downloaded our #debian-malaysia Archive log and paste it on [salsa.debian.net](https://salsa.debian.org/malaysia-team) as snippet and you can have a look [here](https://salsa.debian.org/malaysia-team/malaysia-team.pages.debian.net/-/snippets/587).

It not much but it is a good way to keep it as a reference and as our collection . 