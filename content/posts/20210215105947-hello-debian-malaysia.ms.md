+++ 
draft = false
date = 2021-02-15T17:59:47+08:00
title = "Salam perkenalan daripada Debian Malaysia"
description = ""
slug = "hello-debian-malaysia"
authors = ['Debian Malaysia <debianmalaysia AT SPAMFREE gmail DOT com>']
tags = ['berita', 'debian', 'sesawang', 'malaysia', 'hugo', 'linux', 'sumber terbuka', 'debian malaysia']
categories = []
externalLink = ""
series = []
+++

## Pengenalan daripada Debian Malaysia

Hai semua! Selamat datang ke laman sesawang "Debian Malaysia". Adalah lebih baik untuk kami kenalkan serba sediki mengenai kami kepada semua :)

Jom berbicara mengenai "Linux", Linux adalah kernel itu sendiri dan ianya adalah sebahagian daripada sistem operasi. Kernel terhasil dan datang daripada perisian GNU dan perisian tambahan yang lain. Ianya memberi adakala diberi nama GNU/Linux dari sinilah terhasilnya pelbagai variasi distribusi linux. Debian yang juga dikenali sebagai Debian GNU/Linux adalah salah satu sistem operasi tertua bersandarkan Linux kernel.

Debian adalah sebuah distro yang indah, serba boleh dan padu. Ia dikendalikan oleh komuniti yang demokratik dan tidak ada politik syarikat tertentu. Projek Debian di asaskan oleh Ian Murdock pada 16 Ogos 1993 dan projek ini masih diteruskan dan berjalan sehingga kini kerana ianya dikembangkan oleh komuniti diseluruh dunia. Projek ini diselaraskan melalui internet oleh sekumpulan sukarelawan dan dibimbing oleh Debian Project Leader (DPL).

Ia mempunyai komuniti yang hebat dan kami sebagai "Debian Malaysia" ingin menjadi sebahagian daripadanya dan ingin menyumbang kepada Projek Debian.

Kami mengalu-alukan semua rakyat Malaysia atau orang asing yang tinggal di Malaysia untuk menjadi sebahagian daripada kami, bergerak sebagai komuniti dan menyumbang kepada gerakan sumber terbuka Malaysia .

#  Latar belakang dan tujuan laman sesawang ini
Laman web ini sebenarnya hanyalah weblog statik yang dihasilkan menggunakan Hugo dan di host pada halaman Gitlab dan salin pada halaman Debian.

Sebab kenapa kita memilih menggunakan laman web statik kerana ianya jauh lebih ringkas, lebih murah dan lebih mudah dikendalikan. Alasan lain yang sangat penting sekali adalah kerana kita dapat menyimpan kod sumber laman ini di dalam repositori kawalan dan semestinya akan mudah untuk sesiapa yang hendak terlibat sekiranya dia ingin menghantar artikel baru, memperbaiki bug atau bahkan mahu mempunyai salinan keseluruhan laman web ini.


Dengan kaedah ini, ia dapat menjamin untuk laman web kami tersedia dalam jangka masa panjang dan dikendalikan sendiri oleh komuniti. Oleh itu, jemputlah datang dan "fork" repo laman web kami dan hantarkan kepada kami "pull request" anda.

Tidak perlu menghabiskan satu sen pun untuk hosting atau domain buat masa ini :)

# Hubungi
Anda boleh menghubungi kami dengan sentiasa melayari laman web ini untuk kemas kini dari semasa ke semasa atau hanya melanggan [feed RSS](../../index.xml) kami. 

Anda semua di jemput untuk bersosial bersama kami melalui irc/matrix, group telegram dan mailing list. Anda semua di jemput!

# Penghargaan terima kasih
Kami ingin mengucapkan jutaan terima kasih kepada sesiapa sahaja yang melibatkan dan menjumpai kami sebelum kami memuncul diri kepada umum. Terima kasih kepada semua kerana menyertai dan menyokong Debian Malaysia dan akhir sekali, terima kasih pada  [Debian Localgroups Team](https://wiki.debian.org/Teams/LocalGroups) yang mengalu-alukan dan menyokong kami.

{{< figure src="../../../images/Powered_by_Debian.svg.png" caption="Debian Malaysia disin!" >}}