+++
title = "Contact"
slug = "contact"
+++

Come and join our [public mailing list](https://groups.google.com/g/debmal) by sending an email to `debmal AT googlegroups DOT com` 🥰 or privately to `debianmalaysia AT gmail DOT com` 🧐. You also can reach the community via our [telegram group](http://t.me/debianmalaysia), [twitter](https://twitter.com/debianmalaysia), [mastodon](https://linuxrocks.online/@debianmalaysia) or [matrix](https://matrix.to/#/#_oftc_#debian-malaysia:matrix.org) / irc ([oftc](irc://irc.oftc.net/debian-malaysia), [~~freenode~~](irc://irc.freenode.org/debian-malaysia) and [kampungchat](irc://irc.kampungchat.org/debian-malaysia)) 🤓