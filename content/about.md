+++
title = "About"
slug = "about"
+++
### WHAT is Debian (ˈdɛbiən)?
{{< figure src="/images/Powered_by_Debian.svg.png" caption="Powered by Debian!" >}}
Debian aka Debian GNU/Linux is a Linux distribution composed of free and open-source software, developed by the community-supported Debian Project. The Debian project is an association of individuals who have made common cause to create a free operating system. Please read [here](https://www.debian.org/intro/about#what) for more information.

### WHERE is Malaysia (məlejsiə) located?
{{< figure src="/images/map.PNG" caption="image: https://en.wikipedia.org/wiki/Malaysia" >}}
Malaysia is a developing country of Southeast Asia, lying just north of the Equator, that is composed of two noncontiguous regions: Peninsular Malaysia (Semenanjung Malaysia), which is on the Malay Peninsula, and East Malaysia (Malaysia Timur), which is on the island of Borneo.

### WHO are Debian Malaysia? Who are Debmal? WHO are you?
Debmal are previous Debian Malaysia local community, I not sure how they organize but as I see Umarzuki and Garfield are the ones who lead this community together with more people more in behind.

They (Debmal community) did a lot of awesome thing such as physical booth during opensource conference, own website (blog, wiki, media), do IRC meeting, mailing-list (on googlegroup), local apt repository and many more super-duper stuff.

As far I able to recall Debmal exists start from 2009 and disappear on thin air around 2015. There is a lot of reasons and speculation why but we won't talk about that here for now.

Others Malaysia FOSS local group movement during that circa are so active! Even some Malaysian people release the own blends of Debian called as Oshirix (from mambang.org.my community) together with the own self develop software packages together but sadly Oshirix also disappear like the rest.

Fast foward on Jan 2021, a kampungboy name Robbi Nespu want to have a Debian local user group in Malaysia. It depend how you interprate it as it may look he continuing Debmal or he start a new ones, as for him this part does't not important (but for everyone information, he contacted Umarzuki and Garfield about his idea, hoping for bless, supports and no clash of interest in future).

To be honest, he aware there is no profit doing this local group community such thing and nowadays everyone busy with personal matter and real job but he decide to continue build the community and plan to let the community grow by itself, move freely, open and last forever. He want to gather anyone who uses or is interested with Debian project, whether it's of forks and derivatives such as Ubuntu, MXLinux, Kali Linux, Knoppix, Devuan  or any others. This is open community and everyone is welcome as long you follow the Debian CoC (code of conduct) and does't break the country laws.

The funky part are eventually debian-usergroup team are not well established / unsorted on Debian side but during DebConf20, Jonathan Cristopher Carter (jcc) and Moray Allan (morray) talk about they plan setup debian-localgroups team properly. You maybe interested to see the talk on [https://debconf20.debconf.org/talks/50-local-teams](https://debconf20.debconf.org/talks/50-local-teams). So lets take it as "Debian Malaysia" localgroup as guinea pig / test subject for localgroups team to figure out the process of assisting, administer/approve budgets and all thus kind of stuff.

So, Robbi choose "Debian Malaysia" as marketing tagline because this two words are what human and machine readable (Ehem..I mean the search engine, please do not imagine terminator or alien space robot etc) which is understandable and relates each others.

His mission is to gather people from `I've-never-heard-of-linux` all the way to super lagendary level. Debian Malaysia will be as center of point for Malaysian people who want to get help, promoting or geting involve with Debian and other FOSS stuff.

### How I can join Debian Malaysia?
Simply say "Hi" and tell about yourself to any of our communication channel

### How to keep in touch / contact Debian Malaysia?
Do you have any questions or suggestions for us? Feel free to join our [telegram group](https://t.me/debianmalaysia), open an issue on [Gitlab](https://gitlab.com/debianmalaysia/debianmalaysia.gitlab.io/-/issues/new) (if related to this website), post an email to [debmal mailing list](mailto:debmal@googlegroups.com), tweets on [Twitter](https://twitter.com/debianmalaysia), tooths on [Mastodon](https://linuxrocks.online/@debianmalaysia) or chat with us on Freenode IRC `#debian-malaysia` channel. Don't forget to check this website regularly and subcribe to our [RSS](../index.xml) feed.


Thanks for reading!
