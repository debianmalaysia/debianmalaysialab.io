+++
title = "Hubungi"
slug = "hubungi"
+++

Jemputlah menyertai kami di [mailing list umum](https://groups.google.com/g/debmal) dengan menghantar emel ke `debmal AT googlegroups DOT com` 🥰 atau secara sulit melalui emel `debianmalaysia AT gmail DOT com` 🧐. Anda juga boleh bersama-sama dengan kami melalui [telegram group](http://t.me/debianmalaysia), [twitter](https://twitter.com/debianmalaysia), [mastodon](https://linuxrocks.online/@debianmalaysia) atau [matrix](https://matrix.to/#/#_oftc_#debian-malaysia:matrix.org) / irc ([oftc](irc://irc.oftc.net/debian-malaysia), [~~freenode~~](irc://irc.freenode.org/debian-malaysia) dan [kampungchat](irc://irc.kampungchat.org/debian-malaysia)) 🤓